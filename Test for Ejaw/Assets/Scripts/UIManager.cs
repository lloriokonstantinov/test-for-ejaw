﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Button startButton = null;
    [SerializeField] private Button pauseButton = null;
    [SerializeField] private Text timerText = null;
    [SerializeField] private Text scoreText = null;

    private void Awake()
    {
        startButton.onClick.AddListener(StartClick);
        pauseButton.onClick.AddListener(PauseClick);
        GameManager.Instance.OnScoreChanged += ScoreUpdate;
        GameManager.Instance.OnTimerChanged += TimerUpdate;
        GameManager.Instance.OnGameOver += GameOver;
        pauseButton.gameObject.SetActive(false);
    }

    private void StartClick()
    {
        GameManager.Instance.StartGame();
        startButton.gameObject.SetActive(false);
        pauseButton.gameObject.SetActive(true);
    }

    public void PauseClick()
    {
        if (GameManager.Instance.IsPaused)
        {
            GameManager.Instance.ResumeGame();
            startButton.gameObject.SetActive(false);
        }
        else
        {
            GameManager.Instance.PauseGame();
            startButton.gameObject.SetActive(true);
        }
    }

    private void ScoreUpdate(float score)
    {
        scoreText.text = score.ToString("0");
    }

    private void GameOver()
    {
        startButton.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(false);
    }

    private void TimerUpdate(float timer)
    {
        timerText.text = timer.ToString("0");
    }

}
