﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    #region Singletone
    private static Spawner instance;
    public static Spawner Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Spawner>();
            }
            return instance;
        }
    }
    #endregion

    [SerializeField][Range(0f,1f)] private float badShapeProbabitity = 0.3f;
    [SerializeField] private float badScoreAmount = -5;
    [SerializeField] private Color badColor = Color.red;


    [SerializeField] private Color[] goodShapeColors = null;
    [SerializeField] private float goodScoreAmountMax = 5;
    [SerializeField] private float goodScoreAmountMin = 1;

    [SerializeField] private float spawnRate = 2;
    [SerializeField] private int spawnCount = 3;
    [SerializeField] private float shapeLifeTime = 4;

    [SerializeField] private float borderOffset = .9f;
    private Vector3 upperLeftCorner;
    private Vector3 downRightCorner;

    private List<TappingShape> activeShapes;

    private Coroutine spawnCoroutine;
    private void Awake()
    {
        activeShapes = new List<TappingShape>();
    }

    public void StartSpawn()
    {
        SetSpawnBorders();
        spawnCoroutine = StartCoroutine(SpawnCoroutine());
    }

    public void Refresh()
    {
        if (spawnCoroutine != null)
        {
            StopCoroutine(spawnCoroutine);
        }

        while(activeShapes.Count > 0)
        {
            activeShapes[0].Remove();
        }
        
    }

    private IEnumerator SpawnCoroutine()
    {
        var waiter = new WaitForSeconds(spawnRate);

        while (true)
        {
            yield return waiter;
            for (int i = 0; i < spawnCount; i++)
            {
                SpawnShape();
            }
        }
    }

    private void SpawnShape()
    {
        var sprite = ResourseManager.SpriteShapes[Random.Range(0, ResourseManager.SpriteShapes.Count)];
        Color color;
        float score;
        if (Random.value < badShapeProbabitity)
        {
            color = badColor;
            score = badScoreAmount;
        }
        else
        {
            color = goodShapeColors[Random.Range(0, goodShapeColors.Length)];
            score = Mathf.Round(Random.Range(goodScoreAmountMin, goodScoreAmountMax));
        }
        var x = Random.Range(upperLeftCorner.x, downRightCorner.x);
        var y = Random.Range(upperLeftCorner.y, downRightCorner.y);

        var newShape = Pool.Instance.Take("shape") as TappingShape;
        activeShapes.Add(newShape);
        newShape.Initalize(sprite, color, score, shapeLifeTime, new Vector3(x, y), () => activeShapes.Remove(newShape) );
    }

    private void SetSpawnBorders()
    {
        Vector3[] frustumCorners = new Vector3[4];
        Camera.main.CalculateFrustumCorners(new Rect(0, 0, 1, 1), Camera.main.farClipPlane * borderOffset, Camera.MonoOrStereoscopicEye.Mono, frustumCorners);
        upperLeftCorner = Camera.main.transform.TransformVector(frustumCorners[0]);
        downRightCorner = Camera.main.transform.TransformVector(frustumCorners[2]);
    }



}
