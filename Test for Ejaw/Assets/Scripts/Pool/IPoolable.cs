﻿using UnityEngine;

public interface IPoolable
{

    string Name { get; }

    void ReturnToPool(Transform poolParent);

    void Activate();
}
