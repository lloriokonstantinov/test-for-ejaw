﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// Pool data structure
/// </summary>
public class Pool : MonoBehaviour
{
    #region Singletone
    private static Pool instance;
    public static Pool Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new GameObject("Pool").AddComponent<Pool>();
            }
            return instance;
        }
    }
    #endregion

    private Transform basePoolParent;

    private Dictionary<string, MonoBehaviour> knownPrefabs;
    private Dictionary<string, Stack<IPoolable>> pool;

    private void Awake()
    {
        basePoolParent = transform;

        knownPrefabs = new Dictionary<string, MonoBehaviour>();
        pool = new Dictionary<string, Stack<IPoolable>>();

    }

    public IPoolable Take(string type)
    {
        if (knownPrefabs.ContainsKey(type))
        {
            IPoolable poolObject;
            if (pool[type].Count != 0)
            {
                poolObject = pool[type].Pop();
            }
            else
            {
                var prefab = knownPrefabs[type];
                poolObject = Instantiate(prefab, basePoolParent) as IPoolable;
            }
            poolObject.Activate();
            return poolObject;
        }
        return null;
    }

    public void InitObject(IPoolable poolObject, int count)
    {
        if (!knownPrefabs.ContainsKey(poolObject.Name))
        {
            var monoBeh = poolObject as MonoBehaviour;
            knownPrefabs.Add(poolObject.Name, monoBeh);
            pool.Add(poolObject.Name, new Stack<IPoolable>());

            for (int i = 0; i < count; i++)
            {
                var instance = Instantiate(monoBeh, basePoolParent) as IPoolable;
                instance.ReturnToPool(basePoolParent);
                pool[poolObject.Name].Push(instance);
            }
        }
    }

    public void Put(IPoolable poolObject)
    {
        string type = poolObject.Name;
        if (!knownPrefabs.ContainsKey(type))
        {
            return;
        }
        pool[type].Push(poolObject);
        poolObject.ReturnToPool(basePoolParent);
        
    }

}
