﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] private float globalTimerAmount = 60f;


    private float timer;
    public float Timer
    {
        get
        {
            return timer;
        }
        private set
        {
            timer = value;
            OnTimerChanged(value);
        }
    }

    private float score;
    public float Score
    {
        get
        {
            return score;
        }
        private set
        {
            score = value;
            OnScoreChanged(value);
        }
    }

    public bool IsPaused { get; private set; }

    public event Action<float> OnScoreChanged = (amount) => { };
    public event Action<float> OnTimerChanged = (amount) => { };
    
    public event Action OnGameOver = () => { };
    #region Singletone
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }
    #endregion

    private void Awake()
    {
        ResourseManager.InitResources();
        IsPaused = true;
    }

    public void StartGame()
    {
        Score = 0f;       
        Spawner.Instance.Refresh();
        Spawner.Instance.StartSpawn();
        StartCoroutine(TimerRunning());
        ResumeGame();
    }

    public IEnumerator TimerRunning()
    {
        Timer = globalTimerAmount;
        var waiter = new WaitForEndOfFrame();
        while (Timer > 0)
        {
            Timer -= Time.deltaTime;
            OnTimerChanged(Timer);
            yield return waiter;
        }
        PauseGame();
        OnGameOver();
    }

    public void PauseGame()
    {
        Time.timeScale = 0f;
        IsPaused = true;
    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        IsPaused = false;
    }

    public void AddScore(float amount)
    {
        Score += amount;
    }
}
