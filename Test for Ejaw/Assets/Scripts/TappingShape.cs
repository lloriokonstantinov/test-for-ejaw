﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TappingShape : MonoBehaviour, IPoolable
{
    [SerializeField] private SpriteRenderer spriteRenderer = null;

    private float points;

    private float lifeTime;

    public event Action OnRemoved = () => { };

    #region IPoolable

    [SerializeField] private string poolName = null;

    string IPoolable.Name { get { return poolName; } }

    void IPoolable.Activate()
    {
        transform.SetParent(null);
        gameObject.SetActive(true);
    }

    void IPoolable.ReturnToPool(Transform poolParent)
    {
        transform.SetParent(poolParent);
        gameObject.SetActive(false);
    }
    #endregion

    public void Initalize(Sprite newSprite, Color newColor, float newPoints, float newLifeTime, Vector3 position, Action onRemove = null)
    {
        spriteRenderer.sprite = newSprite;
        spriteRenderer.color = newColor;
        points = newPoints;
        lifeTime = newLifeTime;
        transform.position = position;
        OnRemoved += onRemove;
        StartCoroutine(LifeCoroutine());
    }

    private IEnumerator LifeCoroutine()
    {
        yield return new WaitForSeconds(lifeTime);
        Remove();
    }

    private void OnMouseDown()
    {
        if (GameManager.Instance.IsPaused)
        {
            return;
        }
        ActivateEffect();
        Remove();
    }

    public virtual void ActivateEffect()
    {
        GameManager.Instance.AddScore(points);
    }

    public void Remove()
    {
        OnRemoved();
        OnRemoved = () => { };
        StopAllCoroutines();
        Pool.Instance.Put(this);
    }

}
