﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ResourseManager
{

    public static List<Sprite> SpriteShapes;

    public static void InitResources()
    {
        Pool.Instance.InitObject(Resources.Load<TappingShape>("Prefabs/TappingShape"), 15);
        SpriteShapes = new List<Sprite>( Resources.LoadAll<Sprite>("Textures/Shapes"));
    }
}
